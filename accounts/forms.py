from django import forms
from django.contrib.auth.forms import UserCreationForm
from profiles.models import Profile
from committees.models import Committee

# Create your forms here.

class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)  # Make email field compulsory

    class Meta(UserCreationForm.Meta):
        fields = UserCreationForm.Meta.fields + ('email',)

    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True  # Set email field as required

class ProfileForm(forms.ModelForm):
    committees = forms.ModelMultipleChoiceField(queryset=Committee.objects.all(), required=False, widget=forms.CheckboxSelectMultiple)
    resume = forms.FileField(label='Resume', required=False)
    image = forms.ImageField(label='Image', required=False)
    department_year = forms.ChoiceField(choices=Profile.DEPARTMENT_YEAR_CHOICES, required=False)
    blood_group = forms.ChoiceField(choices=Profile.BLOOD_GROUP_CHOICES, required=False)

    class Meta:
        model = Profile
        fields = ['contact_number', 'address', 'birth_date', 'committees', 'resume', 'image', 'department_year', 'blood_group']  # Include blood_group in fields list
        widgets = {
            'birth_date': forms.DateInput(attrs={'type': 'date'}),
            'committees': forms.CheckboxSelectMultiple,
        }