# Generated by Django 5.0.4 on 2024-04-22 19:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("committees", "0004_committee_email"),
    ]

    operations = [
        migrations.AddField(
            model_name="committee",
            name="description",
            field=models.TextField(blank=True, null=True),
        ),
    ]
