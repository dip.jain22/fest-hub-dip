from django.db import models
from django.core.validators import FileExtensionValidator
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from django.db.models.signals import pre_delete
from django.dispatch import receiver
import uuid

class Committee(models.Model):
    name = models.CharField(max_length=100)
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    banner = models.ImageField(
        verbose_name='Banner',
        upload_to='committees/',
        default='defaults/event_banner.webp',
        validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png', 'gif', 'webp'])]) 
    website= models.URLField(max_length=200, blank=True, null=True)
    instagram= models.URLField(max_length=200, blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True, validators=[EmailValidator()])
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name
