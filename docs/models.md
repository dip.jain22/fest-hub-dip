# Models

## Student
### Uid
- PrimaryKeyConstraint

### Username
- CharField,unique

### Password
- password

### Name
- CharField

### Class
- CharField

### Contact
- CharField

### Likes_count
- IntegerField

### Comments_count
- IntegerField

### Following
- ManyToManyField('self', symmetrical=False)

### Participated_in
- ForeignKey(Event, on_delete=models.CASCADE)

## Committee
### C_id
- PrimaryKeyConstraint

### Name
- CharField

### Username
- CharField,unique

### Password
- password

### Website
- URLField

### Insta_id
- CharField

### Whatsapp_grp
- CharField

### Follower_count
- IntegerField

### Organized_events
- ForeignKey(Event, on_delete=models.CASCADE)

## Event
### E_id
- PrimaryKeyConstraint

### Name
- CharField

### Registration_link
- URLField

### Registration_count
- IntegerField

### Date
- DateField

### Likes
- IntegerField

### Comments
- IntegerField

### Host_committee
- ForeignKey(Committee, on_delete=models.CASCADE)
