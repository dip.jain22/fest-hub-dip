import datetime
from django.db import models
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.contrib.auth.models import User
from committees.models import Committee
from django.db.models.signals import pre_delete
from django.dispatch import receiver
import uuid

def validate_image_size(image):
    file_size = image.file.size
    limit_kb = 512
    if file_size > limit_kb * 1024:
        raise ValidationError("Max size of file is %s KB" % limit_kb)


class Faq(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    question = models.CharField(max_length=200)
    answer = models.TextField()
    event = models.ForeignKey('Event', on_delete=models.CASCADE, related_name='faqs', null=True)

class Rule(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rule = models.TextField()
    event = models.ForeignKey('Event', on_delete=models.CASCADE, related_name='rules', null=True)

class Event(models.Model):
    NATURE_CHOICES = [
        ('TECH', 'Technical'),
        ('NON_TECH', 'Non-Technical'),
        ('CULTURAL', 'Cultural'),
        ('OTHER', 'Other'),
    ]

    LEVEL_CHOICES = [
        ('COLLEGE', 'College Level'),
        ('DEPARTMENT', 'Department Level'),
        ('INTER_COLLEGE', 'Inter-College Level'),
        ('OTHER', 'Other'),
    ]

    EVENT_TYPE_CHOICES = [
        ('INTERNAL', 'Internal'),
        ('EXTERNAL', 'External'),
    ]

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    date_time = models.DateTimeField(default=datetime.datetime(2050, 1, 1, 12, 0, 0))
    venue = models.CharField(max_length=200, default="SPIT")
    prize_pool = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    committees = models.ManyToManyField(Committee, verbose_name="Committee")

    banner = models.ImageField(
        
        upload_to='events/',
        default='defaults/event_banner.webp',
        validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png', 'gif', 'webp'])]  
    )
    
    nature = models.CharField(max_length=20, choices=NATURE_CHOICES, default='OTHER', null=True)
    level = models.CharField(max_length=20, choices=LEVEL_CHOICES, default='OTHER', null=True)
    event_type = models.CharField(max_length=20, choices=EVENT_TYPE_CHOICES, default='OTHER', null=True)
    contact_info = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    website = models.URLField(blank=True,default='#')
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.name
    
    def date_only_field(self):
        return self.datetime_field.date()
    
class UserEvent(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='interests')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='interests')
    pinned = models.BooleanField(default=False)
    registered = models.BooleanField(default=False)

    class Meta:
        unique_together = ['user', 'event']

    def __str__(self):
        return f"{self.user.username}-{self.event.name}"  


