from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views

# Customise admin headings
admin.site.site_header = "FestHub Admin"
admin.site.site_title = "FestHub Admin Portal"
admin.site.index_title = "Welcome to FestHub Admin"


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', views.homepage, name='home'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('registered_events/',views.registered_events, name='registered_events'),
    
    # Event specific paths
    path('events/', include('events.urls')),
    path('committees/', include('committees.urls')),

    # Account specific paths
    path('accounts/', include('accounts.urls')),

    path('about/', views.about, name='about'),
    path('profile/<str:username>/', views.profile, name='profile'),
    path('committee/',views.committee,name='committee')
]

# Serve media files during development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
