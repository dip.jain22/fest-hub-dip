from django.core.exceptions import ObjectDoesNotExist
from profiles.models import Profile

def check_profile_completed(request):
    try:
        profile = Profile.objects.get(user=request.user)
    except ObjectDoesNotExist:
        # If the profile doesn't exist, return False
        return False
        
    return profile.is_profile_complete
