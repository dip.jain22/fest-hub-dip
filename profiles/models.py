from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from committees.models import Committee
from uuid import uuid4
import os

DEPARTMENT_CHOICES = ['CSE', 'EXTC', 'MCA', 'CE']
YEAR_CHOICES = ['1', '2', '3', '4']

class Profile(models.Model):
    DEPARTMENT_YEAR_CHOICES = [
        (department + ' ' + year, department + ' ' + year)
        for department in DEPARTMENT_CHOICES
        for year in YEAR_CHOICES
        if not (department == 'MCA' and (year == '3' or year == '4'))
    ]

    BLOOD_GROUP_CHOICES = [
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    ]

    unique_id = models.CharField(max_length=10, default=uuid4, editable=False, unique=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    contact_number = models.CharField(verbose_name='Contact Number', max_length=16, blank=True)
    resume = models.FileField(upload_to='resumes/', blank=True, null=True)
    image = models.ImageField(upload_to='profile_images/', blank=True, null=True)
    blood_group = models.CharField(max_length=3, choices=BLOOD_GROUP_CHOICES, blank=True, null=True)
    department_year = models.CharField(max_length=6, choices=DEPARTMENT_YEAR_CHOICES, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    committees = models.ManyToManyField(Committee, verbose_name='Committees', blank=True) 
    is_profile_complete = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username if self.user else 'Profile with no user'

@receiver(post_delete, sender=User)
def delete_user_profile(sender, instance, **kwargs):
    try:
        profile = Profile.objects.get(user=instance)
        profile.delete()
    except Profile.DoesNotExist:
        pass

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_delete, sender=Profile)
def delete_profile_files(sender, instance, **kwargs):
    if instance.resume:
        if os.path.isfile(instance.resume.path):
            os.remove(instance.resume.path)
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if hasattr(instance, 'profile'):
        instance.profile.save()
